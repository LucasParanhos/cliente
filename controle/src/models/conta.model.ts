export class Conta{
    public id: number
    public usuarioId: number
    public valor: number
    public parceiroComercial: string
    public descricao: string
    public data: Date
    public statusDaConta: number
}