import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  }, {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  }, {
    path: 'auth',
    loadChildren: "./pages/auth/auth.module#AuthModule"    
    // loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule)
  }, {
    path: 'bills',
    loadChildren: "./pages/bills/bills.module#BillsModule"
    // loadChildren: () => import('./pages/bills/bills.module').then(m => m.BillsModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
