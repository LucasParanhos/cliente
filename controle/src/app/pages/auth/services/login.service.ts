import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { HttpClient, HttpParams } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: Observable<User>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController,
    private httpClient: HttpClient
  ) {
    this.isLoggedIn = auth.authState;
  }

  uri: string = "https://localhost:44305/api/usuarios"

  private async showError(message) {
    const ctrl = await this.toast.create({
      message: message,
      duration: 5000,
      color: "danger"
    });

    ctrl.present();

  }


  public createUser(user): Observable<any> {
    return this.httpClient.post(`${this.uri}`,user)
  }

  login(user){
    this.httpClient.get(`${this.uri}`+ '/' + user.email + '/' + user.password, {observe: 'response'})

    .subscribe(response => {
      console.log(response.body)
      localStorage.setItem('body', JSON.stringify(response.body))
    
      if(response.status == 200){
      this.nav.navigateForward("home")
      }
    },
    error => this.showError("Usuário ou senha inválidos")
    );
  }


  recoverPassword(email) {
    this.auth.sendPasswordResetEmail(email)
      .then(() => this.nav.navigateRoot('auth'))
      .catch(error => {
        //Show error message
        this.showError("Email não encontrado");
      });
  }


  logout() {
    this.auth.signOut()
      .then(() => this.nav.navigateRoot('auth'));
  }

}
