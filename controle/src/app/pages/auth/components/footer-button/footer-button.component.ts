import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'footer-button',
  templateUrl: './footer-button.component.html',
  styleUrls: ['./footer-button.component.scss'],
})
export class FooterButtonComponent implements OnInit {
  @Input() texto;
  @Input() link;

  constructor() { }

  ngOnInit() {}

}
