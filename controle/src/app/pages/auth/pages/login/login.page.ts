import { LoginService } from '../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  email;
  password;

  constructor(
    private builder: FormBuilder,
    private service: LoginService,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.loginForm = this.builder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
    this.isUserLoggedIn();
  }

  isUserLoggedIn() {
    this.service.isLoggedIn.subscribe( user => {
      if (user){
        //User is autenticated
        this.nav.navigateForward("home") 
      }
    });
  }

  login() {
    const user = this.loginForm.value;
    
    this.service.login(user);
  }

}
