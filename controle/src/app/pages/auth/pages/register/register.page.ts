import { LoginService } from '../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/models/user.model';

@Component({
  selector: 'register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: LoginService) { }

  ngOnInit() {
    this.registerForm = this.builder.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(19)]],
      surname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirm_password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  createUser() {
    var user = new User();
    user.nome = this.registerForm.get('name').value;
    user.sobrenome = this.registerForm.get('surname').value;
    user.email = this.registerForm.get('email').value;
    user.senha = this.registerForm.get('password').value;
    
    this.service.createUser(user).subscribe( retorno => {
      console.log(retorno);
    });
  }


}
