import { LoginService } from '../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {

  forgotForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: LoginService) { }

  ngOnInit() {
    this.forgotForm = this.builder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  recoverPassword() {
    const form = this.forgotForm.value;

    this.service.recoverPassword(form.email);
  }

}
