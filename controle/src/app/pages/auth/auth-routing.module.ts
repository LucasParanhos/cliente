import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { LoginPage } from './pages/login/login.page';
import { Routes, RouterModule } from '@angular/router';
import { ForgotPage } from './pages/forgot/forgot.page';
import { RegisterPage } from './pages/register/register.page';
import { ComponentModule } from './components/component.module';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      }, {
        path: 'login',
        component: LoginPage
      }, {
        path: 'forgot',
        component: ForgotPage
      }, {
        path: 'register',
        component: RegisterPage
      }
    ]
  }
];

@NgModule({
  declarations: [
    LoginPage,
    ForgotPage,
    RegisterPage
  ],
  imports: [
    RouterModule.forChild(routes),
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentModule
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
