import { BillsService } from './../../services/bills.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

  bills;
  filterValue;
  userId;

  constructor(
    private billsService: BillsService,
    private route:ActivatedRoute
  ) { 
    route.params.subscribe(val => {
      this.ngOnInit()
    });
  }

  ngOnInit() {
    this.userId = localStorage.getItem('body')
    this.userId = JSON.parse(this.userId)
    this.filter();
    this.getAll();
  }

  ngAfterContentInit(){
    console.log("Passou aqui")
  }

  filter() {
    console.log(this.filterValue)
    if (this.filterValue) {
      this.billsService.filter(this.filterValue, this.userId.id).subscribe(x => {
        this.bills = x
      });
    } else {
      this.getAll()
    }
  }

  remove(id) {
    this.billsService.remove(id).subscribe( x => {
      this.getAll()
    });
  }

  getAll(){
    this.billsService.getAll(this.userId.id).subscribe(x => {
      this.bills = x
      console.log(x)
    })
  }
}
