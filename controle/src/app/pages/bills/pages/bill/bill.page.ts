import { NavController } from '@ionic/angular';
import { BillsService } from './../../services/bills.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Conta } from 'src/models/conta.model';
import { Router } from '@angular/router';

@Component({
  selector: 'bill',
  templateUrl: './bill.page.html',
  styleUrls: ['./bill.page.scss'],
})
export class BillPage implements OnInit {
  billForm: FormGroup

  public body;

  constructor(
    private builder: FormBuilder,
    private billsService: BillsService,
    private nav: NavController,
    private router: Router
  ) { }

  ngOnInit() {
    this.initForm();
    this.body = localStorage.getItem('body')
     this.body = JSON.parse(this.body)

     console.log(this.body)
  }

  private initForm() {
    this.billForm = this.builder.group({
      Id: 1,
      ParceiroComercial: ['', [Validators.required, Validators.minLength(3)]],
      Descricao: ['', [Validators.required, Validators.minLength(6)]],
      Valor: ['', [Validators.required, Validators.min(0.01)]],
      StatusDaConta: ['', [Validators.required]],
      Data: ['', [Validators.required]]
    });
  }

  /*
  * Create a new bill on Firebase, in collection "bill"
  */
  async createBill(){
    var conta = new Conta()
    conta.data = this.billForm.get("Data").value
    conta.valor = this.billForm.get("Valor").value
    conta.parceiroComercial = this.billForm.get("ParceiroComercial").value
    conta.descricao = this.billForm.get("Descricao").value
    conta.statusDaConta = this.billForm.get("StatusDaConta").value
    conta.usuarioId = this.body.id
    console.log(conta)

    const data = this.billForm.value;
    this.billsService.createBill(conta).subscribe(() => this.router.navigate(['bills/report']))  


  }
}
