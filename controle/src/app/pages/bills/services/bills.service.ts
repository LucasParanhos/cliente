import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})



export class BillsService {

  collection: AngularFirestoreCollection;

  constructor(
    private db: AngularFirestore,
    private httpClient: HttpClient
  ) {
  }

  uri: string = "https://localhost:44305/api/conta"

  createBill(bill) {
      return this.httpClient.post(`${this.uri}`,bill)
  }

   filter(field, id) {
    console.log(field, id)
    return this.httpClient.get(`${this.uri}/filter/${field}/${id}`)
   }

  getAll(id) {
    return this.httpClient.get(`${this.uri}/${id}`)
  }

  remove(id) {
    return this.httpClient.delete(`${this.uri}/${id}`)
  }
}
