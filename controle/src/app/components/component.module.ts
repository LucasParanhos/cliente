import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderLogoComponent } from './header-logo/header-logo.component';
import { FooterButtonComponent } from './footer-button/footer-button.component';



@NgModule({
  declarations: [
    HeaderLogoComponent,
    FooterButtonComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ],
  exports: [
    HeaderLogoComponent,
    FooterButtonComponent
  ]
})
export class ComponentModule { }
