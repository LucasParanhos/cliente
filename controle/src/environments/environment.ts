// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC45MObjolOjMrlJfWyDOQNbZJB88zD2jw",
    authDomain: "controle-lucas-paranhos.firebaseapp.com",
    databaseURL: "https://controle-lucas-paranhos.firebaseio.com",
    projectId: "controle-lucas-paranhos",
    storageBucket: "controle-lucas-paranhos.appspot.com",
    messagingSenderId: "1060207973275",
    appId: "1:1060207973275:web:37108cfd0100e99702c73e",
    measurementId: "G-45RZEQ6R5Q"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
