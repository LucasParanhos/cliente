﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend.Data;
using backend.Model;
using System;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContaController : ControllerBase
    {
        private readonly backendContext _context;

        public ContaController(backendContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Conta>>> GetConta(int id)
        {
            return await _context.Contas.Where(x => x.UsuarioId == id).ToListAsync();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutConta(int id, Conta conta)
        {
            if (id != conta.Id)
            {
                return BadRequest();
            }

            _context.Entry(conta).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
    


        [HttpPost]
        public async Task<ActionResult<Conta>> PostConta(Conta conta)
        {

            try
            {
                Usuario usuario = _context.Usuario.Find(conta.UsuarioId);
                usuario.ListaDeContas = new List<Conta>();
                usuario.ListaDeContas.Add(conta);
                //_context.Contas.Add(conta);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetConta", new { id = conta.Id }, conta);
            }catch (Exception ex)
            {
                return BadRequest();
            }
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult<Conta>> DeleteConta(int id)
        {
            var conta = await _context.Contas.FindAsync(id);
            if (conta == null)
            {
                return NotFound();
            }

            _context.Contas.Remove(conta);
            await _context.SaveChangesAsync();

            return conta;
        }


        [HttpGet("filter/{field}/{id}")]
        public async Task<ActionResult<IEnumerable<Conta>>> filter(string field, int id)
        {
            List<Conta> contas;
            if (field.Equals("pay"))
            {
                 contas = await _context.Contas.Where(x => x.UsuarioId == id && x.StatusDaConta == 0).ToListAsync();

            }
            else
            {
                contas = await _context.Contas.Where(x => x.UsuarioId == id && x.StatusDaConta == 1).ToListAsync();
            }
            return contas;
        }

    private bool ContaExists(int id)
        {
            return _context.Contas.Any(e => e.Id == id);
        }
    }
}
