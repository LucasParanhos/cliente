﻿using System;
using backend.Enum;

namespace backend.Model
{
    public class Conta
    {
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        public float Valor { get; set; }
        public string ParceiroComercial { get; set; }
        public string Descricao { get; set; }
        public DateTime Data { get; set; }
        public int StatusDaConta { get; set; }
    }
}
