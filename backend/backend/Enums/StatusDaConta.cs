﻿
using Newtonsoft.Json.Converters;
using System.Text.Json.Serialization;

namespace backend.Enum
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum StatusDaConta
    {
       Pagar = 0,
       Receber = 1
    }
}
